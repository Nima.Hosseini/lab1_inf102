package INF102.lab1.triplicate;



import java.util.HashMap;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {
    @Override
    public T findTriplicate(List<T> list) {
		//Create dictionary to sort how many instances each integer will occur, adding +1 if the instances is already in dictionary
        HashMap<T, Integer> Dict = new HashMap<>();
        int n = list.size();
        for (int i = 0; i < n; i++) {
            T variable = list.get(i);
            if (Dict.containsKey(variable)) {
                Dict.put(variable, Dict.get(variable) + 1);
            } else {
                Dict.put(variable, 1);
            }
            if (Dict.get(variable) == 3) {
                return variable;
            }
        }
        return null;
    }
    
}